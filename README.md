Installation et maintenance d'ArchLinux
=======================================

*date de création* : 2018/03/22


Destinataires du projet
-----------------------
Ce n'est pas destiné à être un manuel à usage générique, et certainement pas à servir de documentation qui pourrait remplacer le wiki d'Arch!
C'est juste le suivit, de ce que JE fais sur MES machines.


Ce que le projet fait
---------------------
Gestion des lignes de commandes nécessaire à l'installation. Le but est de pouvoir reproduire la même installation plusieurs fois en en gardant une trace.


Prérequis
---------
- vim
- git
  - GitFlow
- du temps


Documentation
-------------

- [LICENSE](LICENSE) termes de la licence




